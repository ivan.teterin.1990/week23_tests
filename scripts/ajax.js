async function createBigCommunity() {
    const block = document.createElement('section');
    block.className = 'app-section-big-community';
    const blockTitle = document.createElement('h1');
    blockTitle.className = 'app-section-big-community__title';
    blockTitle.innerHTML = 'Big Community of<br>People Like You';
    const blockText = document.createElement('p');
    blockText.className = 'app-section-big-community__text';
    blockText.textContent =
        'We’re proud of our products, and we’re really excited when we get feedback from our users.';
    const connections = document.createElement('div');
    connections.className = 'app-section-big-community__cards';
    block.appendChild(blockTitle);
    block.appendChild(blockText);
    block.appendChild(connections);
    await getSubscribers().then((subscribers) => {
        subscribers.forEach((subscriber) => {
            connections.appendChild(createCard(subscriber));
        });
    });
    document.querySelector('.app-section').after(block);
}

//Create cards inserted in the section "Big community of people like you"
function createCard(subscriber) {
    const card = document.createElement('div');
    card.className = 'app-section-big-community__card';
    const avatar = document.createElement('img');
    avatar.className = 'card-avatar';
    avatar.setAttribute('src', subscriber.avatar.replace('3000', '9000/api'));
    avatar.setAttribute('height', '150');
    avatar.setAttribute('width', '150');
    avatar.setAttribute('alt', 'Avatar');
    const cardText = document.createElement('p');
    cardText.className = 'card-text';
    cardText.textContent = subscriber.id;
    const cardName = document.createElement('h2');
    cardName.className = 'card-name';
    cardName.textContent = subscriber.firstName + ' ' + subscriber.lastName;
    const cardPosition = document.createElement('p');
    cardPosition.className = 'card-position';
    cardPosition.textContent = subscriber.position;
    card.appendChild(avatar);
    card.appendChild(cardText);
    card.appendChild(cardName);
    card.appendChild(cardPosition);
    return card;
}

//Universal function for sending POST requests with content of email input
async function postRequest (content, url) {
    try {
        const response = await fetch(url, {
            mode: 'cors',
            method: 'POST',
            body: JSON.stringify({ email: content }),
            headers: {
                'Content-Type': 'application/json',
            },
        });
        return response;
    } catch (err) {
        console.log('Fail');
        window.alert(err);
    }
}

//Artificial function returning promise after a delay
function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function subscribe(content) {
    const url = 'http://localhost:9000/api/subscribe';
    await timeout(2000);
    return postRequest(content, url);
}

async function unsubscribe(content) {
    const url = 'http://localhost:9000/api/unsubscribe';
    await timeout(2000);
    return postRequest(content, url);
}

async function getSubscribers() {
    const url = 'http://localhost:9000/api/community';
    const response = await fetch(url, {
        mode: 'cors',
        method: 'GET',
        headers: {
            Accept: 'text/json',
        },
    });
    return response.json();
}

export { createBigCommunity, subscribe, unsubscribe };
