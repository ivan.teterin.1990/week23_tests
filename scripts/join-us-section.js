/* eslint-disable max-classes-per-file */
import { validate } from './email-validator.js';
import { subscribe, unsubscribe } from './ajax.js';

let instance = null;

class SingleSection {
    constructor(type) {
        if (!instance) {
            this.section = createSection(type);
            instance = this;
        } else {
            return instance;
        }
    }

    remove() {
        instance = null;
        let elementForRemoval = document.querySelector(
            '.app-section-join-form',
        );
        elementForRemoval.parentNode.removeChild(elementForRemoval);
    }
}

class SectionCreatorFactory {
    create(type) {
        switch (type) {
            case 'standard': {
                return new SingleSection('standard');
            }
            case 'advanced':
                return new SingleSection('advanced');
        }
    }
}

function createSection(type) {
    if (type !== 'advanced' && type !== 'standard') {
        return null;
    }
    const block = document.createElement('section');
    block.className = 'app-section-join-form';
    block.style.backgroundImage =
        'linear-gradient(to top, rgba(65, 65, 65, .5), rgba(65, 65, 65, .5)), url(./assets/images/form.png)';
    let blockTitle = document.createElement('h1');
    blockTitle.className = 'app-title';
    blockTitle.textContent = 'Join Our Program';
    blockTitle.style.color = 'white';
    blockTitle.style.margin = '0px';

    let blockText = document.createElement('p');
    blockText.className = 'app-subtitle';
    blockText.innerHTML =
        'Sed do eiusmod tempor incididunt<br>ut labore et dolore magna aliqua.';
    blockText.style.color = 'white';
    blockText.style.margin = '40px';

    let blockForm = document.createElement('form');
    blockForm.style.display = 'flex';
    blockForm.style.justifyContent = 'center';
    blockForm.style.marginTop = '10px';
    let blockFormInput = document.createElement('input');
    let blockFormButton = document.createElement('button');
    blockForm.appendChild(blockFormInput);
    blockForm.appendChild(blockFormButton);
    blockFormInput.style.placeContent = 'E-mail';
    blockFormInput.style.backgroundColor = 'rgba(255, 255, 255, 0.4)';
    blockFormInput.style.color = 'white';
    blockFormInput.style.width = '400px';
    blockFormInput.className = 'app-section__input';
    blockFormInput.addEventListener('input', saveInputValue);
    blockFormInput.placeholder = 'E-mail';
    blockFormButton.className = 'app-section__button';
    blockFormButton.style.fontSize = '14px';
    blockFormButton.style.height = '36px';
    blockFormButton.style.width = '110px';
    blockFormButton.style.borderRadius = '18px';
    blockFormButton.style.marginLeft = '30px';
    blockFormButton.style.letterSpacing = '1.5px';
    blockFormButton.textContent = 'SUBSCRIBE';
    if (localStorage.getItem('email')) {
        blockFormInput.style.display = 'none';
        blockFormButton.textContent = 'UNSUBSCRIBE';
    }

    block.style.height = '436px';
    block.style.paddingTop = '100px';
    block.style.boxSizing = 'border-box';
    block.appendChild(blockTitle);
    block.appendChild(blockText);
    block.appendChild(blockForm);
    blockForm.addEventListener('submit', (e) => {
        e.preventDefault();
        if (localStorage.getItem('email')) {
            blockFormButton.disabled = true;
            const response1 = unsubscribe(blockFormInput.value);
            response1.then((data) => {
                if (data.status === 200) {
                    blockFormButton.textContent = 'SUBSCRIBE';
                    blockFormInput.style.display = 'initial';
                    localStorage.removeItem('email');
                    localStorage.removeItem('currentValue')
                    blockFormInput.value = '';
                } else {
                    alert(data.statusText);
                }
                blockFormButton.disabled = false;
            });
        } else if (validate(blockFormInput.value)) {
            blockFormButton.disabled = true;
            localStorage.setItem('email', blockFormInput.value);
            const response2 = subscribe(blockFormInput.value);
            response2.then((data) => {
                if (data.status === 200) {
                    blockFormButton.textContent = 'UNSUBSCRIBE';
                    blockFormInput.style.display = 'none';
                } else {
                    alert(data.statusText);
                }
                blockFormButton.disabled = false;
            });
        } else {
            alert('Please enter a valid email');
        }
    });
    if (type === 'advanced') {
        blockFormButton.style.width = '300px';
        blockFormButton.textContent = 'SUBSCTIBE TO OUR ADVANCED PROGRAM';
    }
    blockFormInput.addEventListener('input', saveInputValue);
    return block;
}

function createJoinUsSection() {
    const factory = new SectionCreatorFactory();
    const newSection = factory.create('standard').section;
    document.querySelector('#footer').before(newSection);
}

function removeJoinUsSection() {
    instance.remove();
}

function saveInputValue() {
    localStorage.setItem(
        'currentValue',
        document.querySelector('.app-section__input').value,
    );
}

function restoreInputValue() {
    document.querySelector('.app-section__input').value =
        localStorage.getItem('currentValue');
}

export {
    createJoinUsSection,
    removeJoinUsSection,
    instance,
    restoreInputValue,
    saveInputValue,
};
