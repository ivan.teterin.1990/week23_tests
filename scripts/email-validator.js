/* eslint-disable arrow-parens */
/* eslint-disable no-extra-semi */
/* eslint-disable import/prefer-default-export */
const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

function validate(email) {
    let result = false;
    VALID_EMAIL_ENDINGS.forEach((end) => {
        if (email.match(new RegExp(`@${end}$`)) instanceof Object) {
            result = true;
        }
    });
    return result;
}

export { validate };
