import { createJoinUsSection, restoreInputValue } from './join-us-section.js';
import { createBigCommunity } from './ajax.js';
import '../styles/style.css';

window.addEventListener('load', createJoinUsSection);
window.addEventListener('load', restoreInputValue);
window.addEventListener('load', createBigCommunity);
