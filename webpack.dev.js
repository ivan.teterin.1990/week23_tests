const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require("path");

module.exports = merge( common, {
  mode: "development",
  devServer: {
    static: path.join(__dirname, 'dist'),
    open: true,
    port: 9000,
    proxy: {
      '/api': {
        target: 'http://localhost:3000',
        pathRewrite: { '^/api': '' },
      },
    },
  },
});
